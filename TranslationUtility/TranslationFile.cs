﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TranslationUtility
{
    public class TranslationFile
    {
        private string _filePath;
        public string RelativePath { get; private set; }

        private JObject _translationObject;

        public TranslationFile(string file) : this(file, JObject.Parse(File.ReadAllText(file))) { }

        public TranslationFile(string path, JObject translationObject)
        {
            if (string.IsNullOrEmpty(path))
                throw new System.ArgumentException($"'{nameof(path)}' cannot be null or empty", nameof(path));

            if (translationObject is null)
                throw new System.ArgumentNullException(nameof(translationObject));

            _filePath = path;
            Init(translationObject);
            var checkPath = Path.GetDirectoryName(_filePath);
            RelativePath = AppSettings.Instance.Path.Length < checkPath.Length ? checkPath.Substring(AppSettings.Instance.Path.Length).TrimStart('\\') : checkPath;
        }
        public bool HasChanges { get; set; }

        public Dictionary<string, JValue> Fields {get; private set;}

        private void Init(JObject translationObject)
        {
            _translationObject = translationObject;
            _translationObject.PropertyChanged += _translationObject_PropertyChanged;
            Fields = _translationObject.Descendants().OfType<JValue>().Where(j => j.Type == JTokenType.String)
                .ToDictionary(p => p.Path, val=>val);
        }

        public void Save()
        {
            File.WriteAllText(_filePath, _translationObject.ToString());
            HasChanges = false;
        }

        /// <summary>
        /// Clone current Translate into another language.
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        internal TranslationFile Clone(string language)
        {
            var newObject = (JObject)_translationObject.DeepClone();
            return new TranslationFile(Path.Combine(Path.GetDirectoryName(_filePath), $"{language}.json"), newObject);
        }

        internal void CopyFrom(TranslationFile translate)
        {
            var oldJsonObject = _translationObject;
            var newJsonObject = (JObject)translate._translationObject.DeepClone();
            foreach (var prop in newJsonObject.Descendants().OfType<JValue>().Where(jval=>jval.Type == JTokenType.String))
            {
                prop.Value = oldJsonObject.SelectToken(prop.Path, false)?.ToString() ?? string.Empty;
            }
            Init(newJsonObject);
        }

        private void _translationObject_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HasChanges = true;
        }

        /// <summary>
        /// Checks that current translation is translated all languages.
        /// </summary>
        /// <param name="translation"></param>
        /// <returns></returns>
        internal bool IsFullyTranslated(JValue translation)
        {
            var translates = TranslateLoader.Current.Translates;
            foreach (var lang in TranslateLoader.Current.OtherVisibleLanguages)
            {
                var otherTranslationFile = translates[lang].FirstOrDefault(t => t.RelativePath == RelativePath);
                if (otherTranslationFile == null || !otherTranslationFile.Fields.TryGetValue(translation.Path, out var property) || string.IsNullOrEmpty(property.Value<string>()))
                    return false;
            }

            return true;
        }
    }

}
