﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TranslationUtility.Properties;

namespace TranslationUtility
{
    public partial class MainForm : Form
    {
        private static NotifyIcon _notifyIcon;
        private bool _loading;
        public MainForm()
        {
            InitializeComponent();
            ValidateForms();
            if (string.IsNullOrWhiteSpace(AppSettings.Instance.Path))
                TryFindTranslations();
            else LoadTranslations(AppSettings.Instance.Path);
            _notifyIcon = notifyIcon;
            
        }

        public static void ShowInfoMessage(string title, string message)
        {
            _notifyIcon.ShowBalloonTip(3000, title, message, ToolTipIcon.Info);
        }

        private void TryFindTranslations()
        {
            var dir = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent;
            if (dir == null) return;
            var standardPath = Path.Combine(dir.FullName, "src\\assets\\i18n");
            if (Directory.Exists(standardPath))
                LoadTranslations(standardPath);
            else
            {
                var i18NDirectory = dir.GetDirectories("i18n", SearchOption.AllDirectories).FirstOrDefault();
                if (i18NDirectory != null)
                {
                    LoadTranslations(i18NDirectory.FullName);
                }
            }
        }

        private void tbPath_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK) return;
            LoadTranslations(folderBrowserDialog.SelectedPath);
        }

        private void ValidateForms()
        {
            cmbMainLanguage.Enabled = btnReload.Enabled = !string.IsNullOrWhiteSpace(AppSettings.Instance.Path);
            tbPath.Text = AppSettings.Instance.Path;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            AppSettings.Instance.Save();
        }

        private void LoadTranslations(string path)
        {
            _loading = true;
            cbHideFilled.Checked = AppSettings.Instance.HideFilled;
            AppSettings.Instance.Path = path;
            btnSave.Enabled = false;
            TranslateLoader.Current.LoadAll();
            if (TranslateLoader.Current.Translates.Count == 0) return;
            cmbMainLanguage.DataSource = TranslateLoader.Current.GetAvailableLanguages();
            cmbMainLanguage.SelectedValue = AppSettings.Instance.DefaultLanguage;
            TranslateLoader.Current.SyncWithDefaultLanguage();
            FillLanguageCheckboxes();
            GenerateRows();
            _loading = false;
        }

        private void FillLanguageCheckboxes()
        {
            panelLanguageSelectors.SuspendLayout();
            panelLanguageSelectors.Controls.Clear();
            var items = TranslateLoader.Current.OtherLanguages.Select(CreateLanguageSwitch).ToList();
                items.Reverse();
                
            panelLanguageSelectors.Controls.AddRange(items.ToArray());
            panelLanguageSelectors.ResumeLayout(true);
        }

        private void ReloadTranslations()
        {
            cbHideFilled.Checked = AppSettings.Instance.HideFilled;
            btnSave.Enabled = false;
            TranslateLoader.Current.LoadAll();
            if (TranslateLoader.Current.Translates.Count == 0) return;
            TranslateLoader.Current.SyncWithDefaultLanguage();
            GenerateRows();
        }

        private void GenerateRows()
        {
            if (TranslateLoader.Current.Translates.Count == 0) return;
            var mainLanguage = AppSettings.Instance.DefaultLanguage;
            panelTranslates.SuspendLayout();
            panelTranslates.Controls.Clear();
            var pos = 0;
            var totalHeight = 0;
            var controls = new List<Control>();
            var width = panelTranslates.Width;

            foreach (var translationFile in TranslateLoader.Current.Translates[mainLanguage])
            {
                var lbl = CreateFileHeaderLabel(translationFile.RelativePath);
                lbl.Top = totalHeight;
                totalHeight += lbl.Height;
                controls.Add(lbl);
                foreach (var translation in translationFile.Fields.Values)
                {
                    if (AppSettings.Instance.HideFilled && translationFile.IsFullyTranslated(translation))
                        continue;
                    var row = new TranslationRow(translationFile, translation) { Width = width, Top = totalHeight, TabIndex = ++pos };
                    row.TextChanged += (s, e) => btnSave.Enabled = true;
                    controls.Add(row);
                    totalHeight += row.Height;
                }
            }
            controls.Reverse();
            panelTranslates.Controls.AddRange(controls.ToArray());
            panelTranslates.ResumeLayout(false);
            ValidateForms();
        }

        private CheckBox CreateLanguageSwitch(string lang)
        {
            var checkbox = new CheckBox
            {
               Dock = DockStyle.Left,
                Text = lang,
                AutoSize = true,
                Checked = !AppSettings.Instance.DisabledLanguages.Contains(lang)
            };
            checkbox.CheckedChanged += (s, e) =>
            {
                TranslateLoader.Current.ShowHideLang(lang, checkbox.Checked);
                GenerateRows();
            };
            return checkbox;
        }
        
        private Label CreateFileHeaderLabel(string text)
        {
            return new Label { Height = 30, Text = text, Dock = DockStyle.Top, TextAlign = ContentAlignment.MiddleCenter, Font = new Font(FontFamily.GenericSansSerif, 16) };
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            TranslateLoader.Current.SaveAllChanges();
            AppSettings.Instance.Save();
            btnSave.Enabled = false;
        }

        private void cbHideFilled_CheckedChanged(object sender, EventArgs e)
        {
            AppSettings.Instance.HideFilled = cbHideFilled.Checked;
            GenerateRows();
        }

        private void btnAutoTranslate_Click(object sender, EventArgs e)
        {
            var rows = new List<TranslationRow>();

            var translationRows = panelTranslates.Controls.OfType<TranslationRow>();
            foreach (var translationRow in translationRows)
            {
                if (translationRow.IsFilled) continue;
                if (translationRow.HasNewLines || string.IsNullOrWhiteSpace(translationRow.MainLanguageText)) continue;
                rows.Add(translationRow);
            }

            var fullText = string.Join(Environment.NewLine, rows.Select(r => r.MainLanguageText));
            var wnd = new AutoTranslate
            {
                Language = TranslateLoader.Current.OtherVisibleLanguages.First(),
                MainText = fullText
            };
            Clipboard.SetText(fullText);
            ShowInfoMessage("Скопировано", "Весь текст скопирован в буфер.");

            if (wnd.ShowDialog() == DialogResult.OK)
            {
                var allOldRows = fullText.Trim().Split(new []{Environment.NewLine}, StringSplitOptions.None);
                var allNewRows = wnd.MainText.Trim().Split(new []{Environment.NewLine}, StringSplitOptions.None);
                if (allOldRows.Length != allNewRows.Length || rows.Count != allOldRows.Length)
                {
                    MessageBox.Show(Resources.MainForm_AutoTranslate_LineCountError);
                    return;
                }

                for (int i = 0; i < rows.Count; i++)
                {
                    rows[i].SetText(allNewRows[i], wnd.Language);
                }
                //GenerateRows();
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(AppSettings.Instance.Path))
            {
                ValidateForms();
                return;
            }

            ReloadTranslations();
        }

        private void cmbMainLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_loading) return;
            var selectedLanguage = cmbMainLanguage.SelectedItem as Language;
            if (selectedLanguage == null) return;
            AppSettings.Instance.DefaultLanguage = selectedLanguage.Id;
            LoadTranslations(AppSettings.Instance.Path);
        }
    }
}
