﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TranslationUtility
{
    class TranslateLoader
    {
        private TranslateLoader() { }
        public static TranslateLoader Current { get; } = new TranslateLoader();
        public Dictionary<string, List<TranslationFile>> Translates { get; private set; } = new Dictionary<string, List<TranslationFile>>();
        public List<string> OtherLanguages { get; set; } = new List<string>();
        public List<string> OtherVisibleLanguages { get; private set; }
        public Dictionary<string, Language> AvailableLanguages { get; private set; } = new Dictionary<string, Language>();

        public List<Language> GetAvailableLanguages()
        {
            return Translates.Keys.Select(t => new Language(t)).ToList();
        }

        public void LoadAll()
        {
            Translates = new Dictionary<string, List<TranslationFile>>();
            if (string.IsNullOrWhiteSpace(AppSettings.Instance.Path)) return;
            if (!Directory.Exists(AppSettings.Instance.Path)) return;

            foreach (var file in Directory.GetFiles(AppSettings.Instance.Path, "*.json", SearchOption.AllDirectories))
            {
                var lang = Path.GetFileNameWithoutExtension(file).ToLowerInvariant();
                if (!Translates.TryGetValue(lang, out var translations))
                {
                    Translates.Add(lang, translations = new List<TranslationFile>());
                }
                translations.Add(new TranslationFile(file));
            }

            if (string.IsNullOrWhiteSpace(AppSettings.Instance.DefaultLanguage))
                AppSettings.Instance.DefaultLanguage = GetLargestLanguage();
            OtherLanguages = Translates.Keys.Where(lang => lang != AppSettings.Instance.DefaultLanguage).ToList();
            OtherVisibleLanguages = OtherLanguages.Where(lng=>!AppSettings.Instance.DisabledLanguages.Contains(lng)).ToList();
            AvailableLanguages = Translates.Keys.ToDictionary(t => t, t => new Language(t));
        }

        public void SyncWithDefaultLanguage()
        {
            var otherLanguages = Translates.Keys.Where(k => k != AppSettings.Instance.DefaultLanguage).ToList();
            foreach (var translate in Translates[AppSettings.Instance.DefaultLanguage])
            {
                foreach (var otherLanguage in otherLanguages)
                {
                    var otherTranslates = Translates[otherLanguage].FirstOrDefault(t => t.RelativePath == translate.RelativePath);
                    if (otherTranslates == null)
                    {
                        Translates[otherLanguage].Add(translate.Clone(otherLanguage));
                    }
                    else
                    {
                        otherTranslates.CopyFrom(translate);
                    }
                }
            }
        }

        public void SaveAllChanges()
        {
            foreach (var translate in Translates)
            {
                foreach (var translation in translate.Value)
                {
                    if (translation.HasChanges)
                        translation.Save();
                }
            }
        }

        public string GetLargestLanguage()
        {
            return Translates.OrderByDescending(t => t.Value.Sum(l => Translates.Count())).Select(l => l.Key).FirstOrDefault();
        }

        internal void ShowHideLang(string lang, bool show)
        {
            if (show)
                AppSettings.Instance.DisabledLanguages.Remove(lang);
            else AppSettings.Instance.DisabledLanguages.Add(lang);
            OtherVisibleLanguages = OtherLanguages.Where(lng=>!AppSettings.Instance.DisabledLanguages.Contains(lng)).ToList();
        }
    }

}
