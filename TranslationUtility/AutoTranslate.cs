﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TranslationUtility
{
    public partial class AutoTranslate : Form
    {
        public AutoTranslate()
        {
            InitializeComponent();
            cmbLanguage.DataSource = TranslateLoader.Current.OtherLanguages.Select(lang => new Language(lang)).ToList();
        }

        public string MainText
        {
            get => tbText.Text;
            set => tbText.Text = value;
        }

        public string Language
        {
            get => (string)cmbLanguage.SelectedValue;
            set => cmbLanguage.SelectedValue = value;
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
