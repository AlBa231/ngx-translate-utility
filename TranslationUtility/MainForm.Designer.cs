﻿
namespace TranslationUtility
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnReload = new System.Windows.Forms.Button();
            this.cbHideFilled = new System.Windows.Forms.CheckBox();
            this.panelLanguageSelectors = new System.Windows.Forms.Panel();
            this.btnAutoTranslate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.cmbMainLanguage = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPath = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.panelMainLanguage = new System.Windows.Forms.Panel();
            this.panelTranslates = new System.Windows.Forms.Panel();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnReload);
            this.panel1.Controls.Add(this.cbHideFilled);
            this.panel1.Controls.Add(this.panelLanguageSelectors);
            this.panel1.Controls.Add(this.btnAutoTranslate);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.tbPath);
            this.panel1.Controls.Add(this.cmbMainLanguage);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblPath);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnReload
            // 
            resources.ApplyResources(this.btnReload, "btnReload");
            this.btnReload.Name = "btnReload";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // cbHideFilled
            // 
            resources.ApplyResources(this.cbHideFilled, "cbHideFilled");
            this.cbHideFilled.Name = "cbHideFilled";
            this.cbHideFilled.UseVisualStyleBackColor = true;
            this.cbHideFilled.CheckedChanged += new System.EventHandler(this.cbHideFilled_CheckedChanged);
            // 
            // panelLanguageSelectors
            // 
            resources.ApplyResources(this.panelLanguageSelectors, "panelLanguageSelectors");
            this.panelLanguageSelectors.Name = "panelLanguageSelectors";
            // 
            // btnAutoTranslate
            // 
            resources.ApplyResources(this.btnAutoTranslate, "btnAutoTranslate");
            this.btnAutoTranslate.Name = "btnAutoTranslate";
            this.btnAutoTranslate.UseVisualStyleBackColor = true;
            this.btnAutoTranslate.Click += new System.EventHandler(this.btnAutoTranslate_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbPath
            // 
            resources.ApplyResources(this.tbPath, "tbPath");
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Click += new System.EventHandler(this.tbPath_Click);
            // 
            // cmbMainLanguage
            // 
            this.cmbMainLanguage.DisplayMember = "Name";
            this.cmbMainLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cmbMainLanguage, "cmbMainLanguage");
            this.cmbMainLanguage.FormattingEnabled = true;
            this.cmbMainLanguage.Name = "cmbMainLanguage";
            this.cmbMainLanguage.ValueMember = "Id";
            this.cmbMainLanguage.SelectedIndexChanged += new System.EventHandler(this.cmbMainLanguage_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblPath
            // 
            resources.ApplyResources(this.lblPath, "lblPath");
            this.lblPath.Name = "lblPath";
            // 
            // panelMainLanguage
            // 
            resources.ApplyResources(this.panelMainLanguage, "panelMainLanguage");
            this.panelMainLanguage.Name = "panelMainLanguage";
            // 
            // panelTranslates
            // 
            resources.ApplyResources(this.panelTranslates, "panelTranslates");
            this.panelTranslates.Name = "panelTranslates";
            // 
            // notifyIcon
            // 
            resources.ApplyResources(this.notifyIcon, "notifyIcon");
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelTranslates);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.ComboBox cmbMainLanguage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Panel panelMainLanguage;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panelTranslates;
        private System.Windows.Forms.Panel panelLanguageSelectors;
        private System.Windows.Forms.CheckBox cbHideFilled;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Button btnAutoTranslate;
        private System.Windows.Forms.Button btnReload;
    }
}

