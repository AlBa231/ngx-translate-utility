﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace TranslationUtility
{
    public partial class TranslationRow : UserControl
    {
        private readonly TranslationFile _translationFile;
        private readonly JValue _mainField;
        private readonly string _jsonPath;
        private static readonly Pen BottomLinePen = Pens.LightGray;
        private static readonly Font TextBoxFont = new Font(FontFamily.GenericSansSerif, 14);
        private bool _firstInit = true;
        private Dictionary<string, TextBox> _textBoxes;

        public bool IsFilled { get; set; }
        public bool HasNewLines => _mainField.Value<string>().Contains("\n");
        public string MainLanguageText => _mainField.Value<string>();

        public TranslationRow()
        {
            InitializeComponent();
        }

        public TranslationRow(TranslationFile translationFile, JValue mainField) : this()
        {
            _translationFile = translationFile ?? throw new ArgumentNullException(nameof(translationFile));
            _mainField = mainField ?? throw new ArgumentNullException(nameof(mainField));
            _jsonPath = _mainField.Path;
            Dock = DockStyle.Top;
            BackColor = Color.White;
            Height = 75;
           //Width = 1920;
            //InitControls();
        }
        
        
        private void InitControls()
        {            
            SuspendLayout();
            Controls.Add(CreateEditTextBox(_mainField, true));
            _textBoxes = new Dictionary<string, TextBox>();
            var translates = TranslateLoader.Current.Translates;
            var pos = 0;
            IsFilled = true;
            if (TranslateLoader.Current.OtherVisibleLanguages.Count == 0) return;
            var width = Width / TranslateLoader.Current.OtherVisibleLanguages.Count;
            foreach (string lang in TranslateLoader.Current.OtherVisibleLanguages)
            {
                var otherTranslationFile = translates[lang].FirstOrDefault(t => t.RelativePath == _translationFile.RelativePath);
                if (otherTranslationFile == null)
                    translates[lang].Add(otherTranslationFile = _translationFile.Clone(lang));

                otherTranslationFile.Fields.TryGetValue(_jsonPath, out var property);

                var textBox = CreateEditTextBox(property, false);
                textBox.Tag = otherTranslationFile;
                textBox.Top = 50;
                textBox.Left = pos++ * width;
                textBox.TabIndex = (pos + 1);
                textBox.Width = width - 25;
                _textBoxes.Add(lang, textBox);
                //textBox. = _mainField.Value.ToString();
                Controls.Add(textBox);
            }
            ResumeLayout(false);
        }
        
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (_mainField == null) return;
            UpdateTextWidth();
        }

        private void UpdateTextWidth()
        {
            if (Controls.Count == 0) return;
            if (TranslateLoader.Current.OtherVisibleLanguages.Count == 0) return;
            Controls[0].Width = Width;
            var width = Width / TranslateLoader.Current.OtherVisibleLanguages.Count;
            for (int i = 1; i < Controls.Count; i++)
            {
                var textBox = (TextBox)Controls[i];
                textBox.Left = (i - 1) * width;
                textBox.Width = width - 25;
            }
        }

        private TextBox CreateEditTextBox(JValue translate, bool grayBackground)
        {
            var textBox = new TextBox
            {
                Top = 25,
                Width = Width,
                TabStop = !grayBackground,
                BorderStyle = BorderStyle.None,
                Text = translate.Value<string>(),
                ForeColor = grayBackground ? Color.LightGray : Color.Black,
                Font = TextBoxFont,
                Tag = translate
            };
            if (!grayBackground)
            {
                textBox.BackColor = string.IsNullOrEmpty(textBox.Text) ? Color.Linen : Color.White;
            }
            else
            {
                textBox.Click += (s, e) =>
                {
                    if (s is TextBox tb) {
                        Clipboard.SetText(tb.Text);
                        MainForm.ShowInfoMessage("Скопировано", tb.Text);
                    }
                };
            }
            textBox.GotFocus += (s, e) => ((TextBox)s).BackColor = Color.LightGoldenrodYellow;
            textBox.LostFocus += (s, e) => ((TextBox)s).BackColor = Color.White;
            textBox.TextChanged += TextBox_TextChanged;
            if (string.IsNullOrEmpty(textBox.Text))
                IsFilled = false;
            return textBox;
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            OnTextChanged(e);
            //btnSave.Enabled = true;
            var textBox = (TextBox)sender;
            if (textBox.Tag is TranslationFile tag)
            {
                //if (textBox.Tag != _mainField)
                {
                    textBox.BackColor = string.IsNullOrEmpty(textBox.Text) ? Color.Linen : Color.White;
                }

                tag.Fields[_jsonPath].Value = textBox.Text;
                
                tag.HasChanges = true;
            }
        }
        
        public void SetText(string text, string lang)
        {
            if (_textBoxes == null)
            {
                _firstInit = false;
                InitControls();
            }
            if (_textBoxes.TryGetValue(lang, out var textbox))
            {
                textbox.Text = text;
            }
            else
            {
                var translation = TranslateLoader.Current.Translates[lang].FirstOrDefault(f => f.RelativePath == _translationFile.RelativePath);
                if (translation != null)
                {
                    translation.Fields[_jsonPath].Value = text;
                    //translation.Fields[_mainField.Path].Value = text;
                    translation.HasChanges = true;
                }
            }
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            if (_firstInit)
            {
                _firstInit = false;
                InitControls();
                Invalidate();
            }
            base.OnPaint(e);
            if (_jsonPath != null)
                e.Graphics.DrawString(_jsonPath, Font, Brushes.Black, 5, 5);
            if (TranslateLoader.Current.OtherVisibleLanguages.Count == 0) return;
            var width = Width / TranslateLoader.Current.OtherVisibleLanguages.Count;
            for (int i = 0; i < TranslateLoader.Current.OtherVisibleLanguages.Count; i++)
            {
                string lang = TranslateLoader.Current.OtherVisibleLanguages[i];
                e.Graphics.DrawString(lang, Font, Brushes.Green, (i + 1)*width - 25, 50);
            }
            e.Graphics.DrawLine(BottomLinePen, 0, Height -1, Width, Height - 1);
        }
    }
}
