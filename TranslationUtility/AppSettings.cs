﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace TranslationUtility
{
    public class AppSettings
    {
        public string Path { get; set; }

        public string DefaultLanguage { get; set; }
        public List<string> DisabledLanguages { get; set; } = new List<string>();
        public bool HideFilled { get; set; }


        public static AppSettings Instance {get;} = Load();

        public void Save()
        {
            var settingsPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json");
            if (System.IO.File.Exists(settingsPath))
            {
                var bakSettingsPath = settingsPath + ".bak";
                if (System.IO.File.Exists(bakSettingsPath))
                {
                    System.IO.File.Delete(bakSettingsPath);
                }
                System.IO.File.Move(settingsPath, bakSettingsPath);
            }
            System.IO.File.WriteAllText(settingsPath, JsonConvert.SerializeObject(this));
        }

        private static AppSettings Load()
        {
            var settingsPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "appsettings.json");
            if (!System.IO.File.Exists(settingsPath)) return new AppSettings();
            try
            {
                return JsonConvert.DeserializeObject<AppSettings>(System.IO.File.ReadAllText(settingsPath));
            }
            catch(Exception e)
            {
                if (MessageBox.Show("Error to open settings file. Reset settings?\r\n\r\n" + e.Message, "Error", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    throw;
                return new AppSettings();
            }
        }
    }
}
