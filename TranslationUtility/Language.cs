﻿namespace TranslationUtility
{
    class Language
    {
        public string Name => GetName();
        public string Id { get; set; }

        public Language(string id)
        {
            Id = id;
        }

        private string GetName()
        {
            switch (Id)
            {
                case "ru":
                    return "Русский";
                case "uk":
                    return "Українська";
                case "en":
                    return "English";
                default:
                    return Id;
            }
        }
    }
}
